import { hasKey } from "src/utils/global";

test("object has property", async () => {
  expect(hasKey({ "my-key": undefined }, "my-key")).toEqual(true);
  expect(hasKey({ "other-key": undefined }, "my-key")).toEqual(false);
});

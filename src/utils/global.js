export const hasKey = (object, key) => {
  return Object.prototype.hasOwnProperty.call(object, key);
};

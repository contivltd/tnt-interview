import {
  API_PARAM_DATE_FROM,
  API_PARAM_DATE_TO,
  STATUS_FINISHED,
  HOME_TEAM,
  AWAY_TEAM,
  HOME_TEAM_KEY,
  AWAY_TEAM_KEY,
  WINNER_DRAW,
} from "src/config";

const DAY_IN_SECONDS = 24 * 60 * 60 * 1000;

/**
 * @param {Date} firstDay started date
 * @param {Int} days number of days
 * @returns {Array} with two Date objects
 */
export const getDateInterval = (firstDay, days) => {
  if (days <= 0) throw new Error("Variable days must be higher than 0");
  const lastDay = new Date(firstDay.getTime() + days * DAY_IN_SECONDS);
  return [firstDay, lastDay];
};

/**
 * @returns {Array} Array of Arrays with time string intervals - for last 30 days
 */
export const getLast30DaysIntervals = () => {
  const today = new Date();
  const day1 = new Date(today.getTime() - 29 * DAY_IN_SECONDS);
  const day11 = new Date(today.getTime() - 19 * DAY_IN_SECONDS);
  const day21 = new Date(today.getTime() - 9 * DAY_IN_SECONDS);
  const part1 = getDateInterval(day1, 9);
  const part2 = getDateInterval(day11, 9);
  const part3 = getDateInterval(day21, 9);
  return [
    [getApiDateFormat(part1[0]), getApiDateFormat(part1[1])],
    [getApiDateFormat(part2[0]), getApiDateFormat(part2[1])],
    [getApiDateFormat(part3[0]), getApiDateFormat(part3[1])],
  ];
};

/**
 * returns correct date format for api
 * @param {Date} datetime
 * @returns date string yyyy-mm-dd
 */
export const getApiDateFormat = (datetime) => {
  if (!datetime instanceof Date) throw new Error("Incorrect variable type.");
  return datetime.toISOString().slice(0, 10);
};

/**
 * Get URLSearchParams object with date params for api
 * @param {String} firstDay date string
 * @param {String} lastDay date string
 * @returns
 */
export const getUrlParamsWithInterval = ([firstDay, lastDay]) => {
  const params = new URLSearchParams();
  params.append(API_PARAM_DATE_FROM, firstDay);
  params.append(API_PARAM_DATE_TO, lastDay);
  return params;
};

/**
 * return winner team object from object match
 * @param {object} match match object from API
 * @returns {object} team object
 */
export const getMatchWinner = (match) => {
  if (match.status === STATUS_FINISHED && match.score.winner !== WINNER_DRAW) {
    switch (match.score.winner) {
      case HOME_TEAM:
        return match[HOME_TEAM_KEY];
      case AWAY_TEAM:
        return match[AWAY_TEAM_KEY];
    }
  }
  return null;
};

/**
 * Extract array of winner teams from matches
 * @param {Array} matches array with matches from api
 * @returns {Array} Array of winners with team. Each team has id, name and numbers of wins
 */
export const extractWinners = (matches) => {
  const winners = [];
  matches
    .filter(
      (match) =>
        match.status === STATUS_FINISHED && match.score.winner !== WINNER_DRAW
    )
    .map((match) => {
      const matchWinner = getMatchWinner(match);
      const existingWinner = winners.find(
        (winner) => winner.name === matchWinner.name
      );
      if (existingWinner) {
        existingWinner.wins += 1;
      } else {
        matchWinner.wins = 1;
        winners.push(matchWinner);
      }
    });
  return winners;
};

/**
 * Sort function for array of winners
 * @param {Array} winners
 * @returns {Array} winners
 */
export const sortByWins = (a, b) => b.wins - a.wins;

import {
  getDateInterval,
  getLast30DaysIntervals,
  getApiDateFormat,
  getUrlParamsWithInterval,
  getMatchWinner,
  extractWinners,
  sortByWins,
} from "./football-api";
import {
  API_PARAM_DATE_FROM,
  API_PARAM_DATE_TO,
  STATUS_FINISHED,
  HOME_TEAM,
  AWAY_TEAM,
  HOME_TEAM_KEY,
  AWAY_TEAM_KEY,
} from "src/config";

const DAY_IN_SECONDS = 24 * 60 * 60 * 1000;

/**
 * sortByWins
 */
test("sortByWins", async () => {
  const winners = [
    { id: 1, wins: 10 },
    { id: 2, wins: 40 },
    { id: 3, wins: 20 },
  ];
  winners.sort(sortByWins);
  expect(winners[0].id).toEqual(2);
});

/**
 * extractWinners
 */
test("extractWinners", async () => {
  const team1 = {
    id: 10,
    name: "Xyz",
  };
  const team2 = {
    id: 12,
    name: "Abc",
  };
  const matches = [
    {
      status: STATUS_FINISHED,
      score: {
        winner: HOME_TEAM,
      },
      [HOME_TEAM_KEY]: team1,
      [AWAY_TEAM_KEY]: team2,
    },
    {
      status: STATUS_FINISHED,
      score: {
        winner: AWAY_TEAM,
      },
      [HOME_TEAM_KEY]: team1,
      [AWAY_TEAM_KEY]: team2,
    },
    {
      status: STATUS_FINISHED,
      score: {
        winner: HOME_TEAM,
      },
      [HOME_TEAM_KEY]: team1,
      [AWAY_TEAM_KEY]: team2,
    },
  ];
  const winners = extractWinners(matches);
  expect(winners.length).toEqual(2);
  expect(winners[0].id).toEqual(10);
  expect(winners[0].wins).toEqual(2);
  expect(winners[1].id).toEqual(12);
  expect(winners[1].wins).toEqual(1);
});

/**
 * getMatchWinner
 */
test("getMatchWinner", async () => {
  const winner = {
    id: 10,
    name: "Xyz",
  };
  const match = {
    status: STATUS_FINISHED,
    score: {
      winner: HOME_TEAM,
    },
    [HOME_TEAM_KEY]: winner,
    [AWAY_TEAM_KEY]: {},
  };
  expect(getMatchWinner(match)).toEqual(winner);
});

/**
 * getUrlParamsWithInterval
 */
test("getUrlParamsWithInterval", async () => {
  const dayFrom = "2021-01-01";
  const dayTo = "2021-10-10";
  const res = getUrlParamsWithInterval([dayFrom, dayTo]);

  expect(res.get(API_PARAM_DATE_FROM)).toEqual(dayFrom);
  expect(res.get(API_PARAM_DATE_TO)).toEqual(dayTo);
  expect(res.toString()).toEqual(
    `${API_PARAM_DATE_FROM}=${dayFrom}&${API_PARAM_DATE_TO}=${dayTo}`
  );
});

/**
 * getApiDateFormat
 */
test("getApiDateFormat", async () => {
  expect(getApiDateFormat(new Date("2021/12/27"))).toEqual("2021-12-27");
});

/**
 * getDateInterval
 */
test("getDateInterval", async () => {
  const startDate = new Date("2021-01-01");
  const days = 7;
  const res = getDateInterval(startDate, days);

  expect(res.length).toEqual(2);
  expect(res[0]).toEqual(startDate);
  expect(res[0].getTime() + days * DAY_IN_SECONDS).toEqual(res[1].getTime());
});

/**
 * getLast30DaysIntervals
 */
test("getLast30DaysIntervals", async () => {
  const today = new Date();
  today.setUTCHours(0, 0, 0, 0);
  const res = getLast30DaysIntervals();
  const daysInterval = 10;

  expect(res.length).toEqual(3);
  expect(res[0].length).toEqual(2);
  expect(res[1].length).toEqual(2);
  expect(res[2].length).toEqual(2);
  expect(new Date(res[2][1]).getTime()).toEqual(today.getTime());
  expect(new Date(res[0][0]).getTime() + daysInterval * DAY_IN_SECONDS).toEqual(
    new Date(res[1][0]).getTime()
  );
  expect(new Date(res[1][0]).getTime() + daysInterval * DAY_IN_SECONDS).toEqual(
    new Date(res[2][0]).getTime()
  );
  expect(
    new Date(res[0][0]).getTime() + (daysInterval * 3 - 1) * DAY_IN_SECONDS
  ).toEqual(new Date(res[2][1]).getTime());
});

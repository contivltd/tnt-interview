/**
 * API
 */
export const FOOTBALL_API_URL = process.env.FOOTBALL_API_URL;
export const FOOTBALL_API_TOKEN = process.env.FOOTBALL_API_TOKEN;
export const API_ENDPOINT_MATCHES = `${FOOTBALL_API_URL}/v2/matches`;

/**
 * url params
 */
export const API_PARAM_DATE_FROM = "dateFrom";
export const API_PARAM_DATE_TO = "dateTo";

/**
 * const
 */
export const STATUS_FINISHED = "FINISHED";
export const WINNER_DRAW = "DRAW";
export const HOME_TEAM = "HOME_TEAM";
export const AWAY_TEAM = "AWAY_TEAM";
export const HOME_TEAM_KEY = "homeTeam";
export const AWAY_TEAM_KEY = "awayTeam";

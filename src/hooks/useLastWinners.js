import { useState, useEffect } from "react";
import apiService from "src/services/ApiService";
import { extractWinners } from "src/utils/football-api";

const useLastWinners = () => {
  const [pending, setPending] = useState(false);
  const [winners, setWinners] = useState([]);

  useEffect(() => {
    const main = async () => {
      setPending(true);
      const res = await apiService.getLastMatchesIn30Days();
      if (res.success) {
        setWinners(extractWinners(res.data));
      }
      setPending(false);
    };
    main();
  }, []);

  return {
    winners,
    pending,
  };
};

export default useLastWinners;

import { ErrorBoundary } from "src/components/organisms";

const withErrorBoundary = (Component) => {
  return (props) => (
    <ErrorBoundary>
      <Component {...props} />
    </ErrorBoundary>
  );
};

export default withErrorBoundary;

import ApiResponse from "src/model/ApiResponse";
import { FOOTBALL_API_TOKEN, API_ENDPOINT_MATCHES } from "src/config";
import {
  getLast30DaysIntervals,
  getUrlParamsWithInterval,
} from "src/utils/football-api";

class FootballDataService {
  /**
   * Singleton
   */
  constructor() {
    if (!FootballDataService.instance) {
      FootballDataService.instance = this;
    }
    return FootballDataService.instance;
  }

  /**
   * private function fetch
   * @param {string} endpoint
   * @returns fetch
   */
  _fetchGET = async (url) =>
    fetch(url, {
      mode: "no-cors",
      headers: {
        "X-Auth-Token": FOOTBALL_API_TOKEN,
      },
    });

  /**
   * get all games in last x days
   */
  getLastMatchesIn30Days = async (days) => {
    const res = new ApiResponse();
    const intervals = getLast30DaysIntervals();
    await Promise.all([
      this._fetchGET(
        `${API_ENDPOINT_MATCHES}?${getUrlParamsWithInterval(
          intervals[0]
        ).toString()}`
      ),
      this._fetchGET(
        `${API_ENDPOINT_MATCHES}?${getUrlParamsWithInterval(
          intervals[1]
        ).toString()}`
      ),
      this._fetchGET(
        `${API_ENDPOINT_MATCHES}?${getUrlParamsWithInterval(
          intervals[2]
        ).toString()}`
      ),
    ])
      .then(async ([res1, res2, res3]) => {
        const data1 = await res1.json();
        const data2 = await res2.json();
        const data3 = await res3.json();
        return [...data1.matches, ...data2.matches, ...data3.matches];
      })
      .then((data) => {
        res.success = true;
        res.data = data;
      })
      .catch((err) => {
        res.success = false;
        res.error = err.message;
      });
    return res;
  };
}

const footballDataService = new FootballDataService();

export default footballDataService;

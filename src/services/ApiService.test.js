import apiService from "./ApiService";

/**
 * footballDataService
 */
test("apiService exists", async () => {
  expect(typeof apiService).toEqual("object");
  expect(typeof apiService.getLastMatchesIn30Days).toEqual("function");
});

import footballDataService from "./FootballDataService";

/**
 * footballDataService
 */
test("footballDataService exists", async () => {
  expect(typeof footballDataService).toEqual("object");
  expect(typeof footballDataService.getLastMatchesIn30Days).toEqual("function");
});

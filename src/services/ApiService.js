import ApiResponse from "src/model/ApiResponse";

class ApiService {
  /**
   * Singleton
   */
  constructor() {
    if (!ApiService.instance) {
      ApiService.instance = this;
    }
    return ApiService.instance;
  }

  /**
   * get all games in last x days
   */
  getLastMatchesIn30Days = async () => {
    const ret = new ApiResponse();
    try {
      const data = await fetch("/api/last-matches").then((response) =>
        response.json()
      );
      ret.success = true;
      ret.data = data;
    } catch (err) {
      ret.success = false;
      ret.error = err.message;
    }
    return ret;
  };
}

const apiService = new ApiService();

export default apiService;

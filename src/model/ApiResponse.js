class ApiResponse {
  constructor() {
    this.success = false;
    this.error = "";
    this.data = null;
  }
}

export default ApiResponse;

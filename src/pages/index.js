import React from "react";
import { Results } from "src/components/templates";
import withErrorBoundary from "src/hoc/withErrorBoundary";
import useLastWinners from "src/hooks/useLastWinners";
import { sortByWins } from "src/utils/football-api";

const Home = () => {
  const { winners, pending } = useLastWinners();

  return (
    <Results
      metaTitle={"Winners table"}
      title={"Most wins in the last 30 days."}
      columns={[
        { key: "id", title: "Team id" },
        { key: "name", title: "Team name" },
        { key: "wins", title: "Number of wins" },
      ]}
      teams={winners.sort(sortByWins).map((w) => ({ ...w, key: w.id }))}
      pending={pending}
    />
  );
};

export default withErrorBoundary(Home);

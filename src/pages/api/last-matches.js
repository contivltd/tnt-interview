import footballDataService from "src/services/FootballDataService";

const handler = async (req, res) => {
  const response = await footballDataService.getLastMatchesIn30Days();
  res.status(200).json(response.data);
};

export default handler;

import React from "react";
import PropTypes from "prop-types";

const Title = ({ as: Component, text, ...props }) => {
  return <Component {...props}>{text}</Component>;
};

Title.propTypes = {
  as: PropTypes.oneOfType([PropTypes.string, PropTypes.elementType]),
  text: PropTypes.string.isRequired,
};

Title.defaultProps = {
  as: "h3",
};

export default Title;

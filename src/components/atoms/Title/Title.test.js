/**
 * @jest-environment jsdom
 */
import React from "react";
import Title from "./Title";
import { render, unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";

let container = null;
beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

/**
 * Render title
 */
it("Render title", () => {
  const text = "title abc";
  const tag = "h1";
  const class1 = "example-class";

  act(() => {
    render(<Title text={text} as={tag} className={class1} />, container);
  });

  expect(Array.from(container.children[0].classList).join(",")).toBe(class1);
  expect(container.children[0].tagName).toBe(tag.toUpperCase());
  expect(container.children[0].tagName).toBe(tag.toUpperCase());
});

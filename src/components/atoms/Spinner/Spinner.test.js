/**
 * @jest-environment jsdom
 */
import React from "react";
import Spinner from "./Spinner";
import { render, unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";

let container = null;
beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

/**
 * Render spinner
 */
it("Render spinner", () => {
  const text = "Loading...";
  act(() => {
    render(<Spinner>{text}</Spinner>, container);
  });

  expect(container.textContent).toBe(text);
});

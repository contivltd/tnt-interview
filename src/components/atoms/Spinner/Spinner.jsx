import React from "react";
import PropTypes from "prop-types";
import styles from "./Spinner.module.scss";

const Spinner = ({ children }) => {
  return <div className={styles.spinner}>{children}</div>;
};

Spinner.propTypes = {
  children: PropTypes.node,
};

Spinner.defaultProps = {
  children: "Loading...",
};

export default Spinner;

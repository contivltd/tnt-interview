import React from "react";
import PropTypes from "prop-types";
import { Title } from "src/components/atoms";
import styles from "./Table.module.scss";

const Table = ({ columns, rows, title }) => {
  return (
    <div className={styles.table}>
      {title && <Title text={title} as="h4" className={styles.table__title} />}
      <table>
        {columns && columns.length && (
          <thead>
            <tr>
              {columns.map((th) => (
                <th key={th.key}>{th.title}</th>
              ))}
            </tr>
          </thead>
        )}
        {rows && rows.length ? (
          <tbody>
            {rows.map((row) =>
              columns && columns.length ? (
                <tr key={row.key}>
                  {columns.map((c) => (
                    <td key={c.key}>{row[c.key]}</td>
                  ))}
                </tr>
              ) : null
            )}
          </tbody>
        ) : null}
      </table>
    </div>
  );
};

Table.propTypes = {
  /**
   * all columns should contain key. Title is optional param for display column name pn table head
   */
  columns: PropTypes.arrayOf(
    PropTypes.shape({
      key: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
      title: PropTypes.string,
    })
  ),
  /**
   * each row should contain key, rest parameters is optional, should be [columnKey]: value
   */
  rows: PropTypes.arrayOf(
    PropTypes.shape({
      key: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    })
  ),
  /**
   * optional title, displayed above table
   */
  title: PropTypes.string,
};

Table.defaultProps = {
  columns: [],
  rows: [],
  title: "",
};

export default Table;

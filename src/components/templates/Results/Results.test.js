/**
 * @jest-environment jsdom
 */
import React from "react";
import Results from "./Results";
import { render, unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";

let container = null;
beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

/**
 * Render
 */
it("Render", () => {
  const text = "Loading...";
  act(() => {
    render(
      <Results
        metaTitle={"Winners table"}
        title={"Most wins in the last 30 days."}
        columns={[
          { key: "id", title: "Team id" },
          { key: "name", title: "Team name" },
          { key: "games", title: "Number of games" },
        ]}
        teams={[
          { key: 0, id: 1, name: "Example 1", games: 13 },
          { key: 1, id: 2, name: "Example 2", games: 7 },
          { key: 2, id: 3, name: "Example 3", games: 25 },
          { key: 3, id: 4, name: "Example 4", games: 2 },
        ]}
        pending={false}
      />,
      container
    );
  });

  expect(container.textContent).toMatch(/Number of games/i);
  expect(container.textContent).toMatch(/Example 3/i);
});

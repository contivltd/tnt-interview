import React from "react";
import styles from "src/styles/Home.module.css";
import Head from "next/head";
import { Table } from "src/components/molecules";
import { Spinner } from "src/components/atoms";
import PropTypes from "prop-types";

const Results = ({ metaTitle, title, columns, teams, pending }) => {
  return (
    <div className={"container"}>
      <Head>
        <title>{metaTitle}</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        {pending ? (
          <Spinner />
        ) : (
          <Table columns={columns} rows={teams} title={title} />
        )}
      </main>

      <footer className={styles.footer}>
        {"Made by Radosław Żaczkiewicz"}
      </footer>
    </div>
  );
};

Results.propTypes = {
  /**
   * meta title
   */
  metaTitle: PropTypes.string,
  /**
   * title displayed above main content
   */
  title: PropTypes.string,
  /**
   * all columns should contain key. Title is optional param for display column name pn table head
   */
  columns: PropTypes.arrayOf(
    PropTypes.shape({
      key: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
      title: PropTypes.string,
    })
  ),
  /**
   * each row should contain key, rest parameters is optional, should be [columnKey]: value
   */
  teams: PropTypes.arrayOf(
    PropTypes.shape({
      key: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    })
  ),
  /**
   * optional - displayed loader when is set as true
   */
  pending: PropTypes.bool,
};

Results.defaultProps = {
  metaTitle: "",
  title: "",
  columns: [],
  teams: [],
  pending: false,
};

export default Results;

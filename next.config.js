module.exports = {
  reactStrictMode: true,
  env: {
    FOOTBALL_API_URL: process.env.FOOTBALL_API_URL,
    FOOTBALL_API_TOKEN: process.env.FOOTBALL_API_TOKEN,
  },
};
